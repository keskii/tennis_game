package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void testFifteenAtThirty() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		//Assert
		assertEquals("Federer fifteen - Nadal thirty", status);
	}

}
